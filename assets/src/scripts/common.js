/* внутренний отступ для body, чтобы контент не залазил под шапку */
$('BODY').css( 'padding-top', $('.header.fixed-top').height() + parseFloat($('.header.fixed-top').css('margin-top')) + parseFloat($('.header.fixed-top').css('margin-bottom')) );
/* \\ внутренний отступ для body, чтобы контент не залазил под шапку */


function resize_mainpage_slider() {
    var $header_height = $('.header').innerHeight();

    $('#mainpage_slider').height( $(window).height() - $header_height );
    $('BODY').css('padding-top', $header_height);
}


/* безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */
$(document).on('click', '[target="_blank"]', function(e) {
    e.preventDefault();

    var otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = $(this).attr('href');
});
/* \\ безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */


/* плавная прокрутка страницы */
$(document).on('click', '[data-scroll-to]', function(e) {
    if ( $( $( $(this).data('scroll-to') ) ).length > 0 ) {
        e.preventDefault();

        // в мобильной версии перед скролом к элементу скрываем меню
        if ( $(window).width() < 768 && $('#header_nav.d-none').length == 0 )
            $('#header_nav').addClass('d-none');

        // в мобильной версии перед скролом к элементу убираем класс .active у кнопки меню навигации
        if ( $(window).width() < 768 && $('[data-toggle="#header_nav"]').hasClass('active') )
            $('[data-toggle="#header_nav"]').removeClass('active');

        $("html, body").animate({
            scrollTop: ( $( $(this).data('scroll-to') ).offset().top - $('header.header').outerHeight(true) ) + "px"
        });
    }
});
/* \\ плавная прокрутка страницы */


/* растягиваем слайдер на главной на всю оставшуюся высоту экрана (высотка экрана - высота шапки) */
$(window).on('resize', function() {
    resize_mainpage_slider();
});
/* \\ растягиваем слайдер на главной на всю оставшуюся высоту экрана (высотка экрана - высота шапки) */


/* скрываем индикатор прокрутки на главной странице при прокрутке страницы */
$(window).on('scroll', function() {
    var indicator_obj = $('#mainpage_slider .icon_scroll-down'),
        delay = 1000;

    if ( $(window).scrollTop() > 0 )
        indicator_obj.fadeOut(delay);
    else
        indicator_obj.fadeIn(delay);
});
/* \\ скрываем индикатор прокрутки на главной странице при прокрутке страницы */


$(document).ready(function() {
    // скрываем прелоадер
    $('#preloader').fadeOut(500);
    // \\ скрываем прелоадер

    /* растягиваем слайдер на главной на всю оставшуюся высоту экрана (высотка экрана - высота шапки) */
    resize_mainpage_slider();
    /* \\ растягиваем слайдер на главной на всю оставшуюся высоту экрана (высотка экрана - высота шапки) */

    // слайдеры
    if ( $('#mainpage_slider.swiper-container .swiper-slide').length > 1 ) {
        var mainpage_slider = new Swiper('#mainpage_slider', {
            centeredSlides: true,
            effect: 'fade',
            speed: 1000,
            autoplay: {
                delay: 10000,
                disableOnInteraction: false,
            },
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

    var mainpage_portfolio_slider = new Swiper('#mainpage_portfolio_slider', {
        slidesPerView: 2,
        spaceBetween: 20,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        /*
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        */
        breakpoints: {
            768: {
                slidesPerView: 4,
                spaceBetween: 42,
            },
            1200: {
                slidesPerView: 5,
                spaceBetween: 42,
            },
        }
    });
    // \\ слайдеры

    
    // сворачивание/разворачивание блоков
    $(document).on('click', '[data-toggle]', function(e) {
        e.preventDefault();

        $( $(this).data('toggle') ).toggleClass('d-none');
        $(this).toggleClass('active');
    });
    // \\ сворачивание/разворачивание блоков

    
    // всплывающие окна
    $.extend(true, $.magnificPopup.defaults, { // изменяем настройки по умолчанию
        tClose: 'Закрыть (Esc)',
        mainClass: 'mfp-with-zoom',
        tLoading: 'Загрузка...',
        gallery: {
            tPrev: 'Предыдущий (Стрелка влево)',
            tNext: 'Следующий (Стрелка вправо)',
            tCounter: '%curr% из %total%'
        },
        image: {
            tError: 'Не удалось загрузить изображение <a href="%url%">#%curr%</a>'
        },
        ajax: {
            tError: 'Не удалось загрузить <a href="%url%">содержимое</a>'
        },
        callbacks: {
            open: function() { // переносим кнопку закрытия в заголовок окна
                $('.popup__header').append( $('.mfp-content .mfp-close') );
            }
            /*
            beforeClose: function() {
                $('.popup__error-msg, .popup__success-msg').text(''); // убираем сообщения
                $('.mfp-content form').trigger('reset'); // чистим форму
            }
            */
        }
    });
      
    $('[data-popup').each(function() {
        var self       = $(this),
            popup_type = $(this).data('popup');
        
        switch ( popup_type ) {
            /*
            case 'photo': // просмотр изображения на весь экран
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            break;
            */
            case 'gallery': // галерея
                self.magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    tLoading: 'Загрузка изображения #%curr%...',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        titleSrc: function(item) {
                            return item.el.attr('alt');
                        }
                    }
                });
            break;
            default: // контент
                self.magnificPopup({
                    type: 'inline'
                });
            break;
        }
    });
    // \\ всплывающие окна


    // ajax запросы при нажатии на кнопки с [data-ajax]
    $(document).on('click', '[data-ajax]', function(e) {
        e.preventDefault();

        var self = $(this),
            action = self.data('ajax'),
            snippet_params = $.parseJSON( self.data('params').replace(/'/g, '"') ),
            // params = {},
            reload = self.data('ajax-reload'),
            form  = ( $('#' + self.attr('form')).length > 0 ) ? $('#' + self.attr('form').get(0)) : // если у элемента с атрибутом data-ajax есть атрибут form получаем связанную с ним форму
                    ( self.parent('form').length > 0 ? self.parent('form').get(0) : // если элемент лежит непосредственно в форме
                    ( self.parentsUntil('form').length > 0 ) ? self.parentsUntil('form').parent().get(0) : null ); // если элемент лежит на любом уровне вложенности в форме

        // собираем данные с формы
        params = new FormData( form );
        params.append( 'action', action );
        for (key in snippet_params)
            params.append( key, decodeURIComponent(snippet_params[key]) );
        
        // отправляем запрос
        $.ajax({
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            dataType: 'json', // тип ожидаемых данных в ответе
            type: 'POST',
            data: params,
            success: function(res) {
                res = $.parseJSON(res);

                // очищаем статусы полей
                form.find('.input.success, .input.error').removeClass('success error');

                var message_timeout = parseInt(res.output.replace(/<[^>]+>/g,'').length) * 75, // задержка в мс зависящая от длины отображаемого сообщения
                    popup = self.parentsUntil('.popup').parent('.popup');

                if ( res.success ) {
                    // выводим сообщение об успешном выполнении запроса
                    form.find('.form__message').removeClass('success error').addClass('success').html(res.output);


                    /* // действия при успешном запросе при определённых action
                    switch ( action ) {
                        case 'auth':
                        case 'logout':
                            // выводим минипрофиль/блок авторизации в шапке сайта
                            $('#header_profile_wrap').html(res.output);

                            // перенаправляем на указанную в res.redirectTo страницу
                            if ( res.redirectTo )
                                window.location = res.redirectTo;
                        break;

                        case 'saveProfile':
                            // обновляем минипрофиль в шапке сайта
                            $('#header_profile_wrap').html(res.header_profile);

                            // выводим сообщение об успешном выполнении
                            form.find('.form__message').removeClass('success error').addClass('success').html(res.output);

                            // через некоторое время (зависящее от длины сообщения) скрываем сообщение
                            setTimeout(function() {
                                form.find('.form__message').text('').removeClass('success error');
                            }, message_timeout);
                        break;
                    }
                    */

                    // чистим список ошибок
                    $('.popup__error-msg').text('');

                    if ( ['reg', 'resetPass'].indexOf(action) == -1 ) { // закрываем попап если это не регистрация и не сброс пароля
                        $.magnificPopup.close();
                    } else {
                        $('.popup__success-msg').text(res.output);
                    }

                    // очищаем поля с паролями
                    form.find('[type="password"]').each(function() {
                        $(this).val('');
                    });
                } else if ( popup && self.parentsUntil('.popup').parent('.popup').length > 0 ) { // если действие вызывалось из попапа и есть ошибки - выводим ошибки в элемент с классом popup__error-msg
                    popup.find('.popup__error-msg').html(res.output);
                } else if ( res.wrong_element_selector ) {
                    // подсвечиваем неправильно заполненные поля или выделяем неправильные элементы
                    if ( !$(res.wrong_element_selector).hasClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error') )
                        $(res.wrong_element_selector).addClass(res.wrong_element_css_class ? res.wrong_element_css_class : 'error');

                    // выводим сообщения об ошибках
                    form.find('.form__message').removeClass('success error').addClass('error').text(res.output);

                    setTimeout(function() {
                        if ( action == 'saveProfile' && !res.success && res.redirectTo ) // при ошибке сохранения профиля пользователя и указании страницы для переадресации - перенаправляем пользователя
                            window.location = res.redirectTo;
                        else
                            form.find('.form__message').text('').removeClass('success error'); // очищаем сообщения формы
                    }, message_timeout);
                }

                // перезагружаем страницу
                if ( res.reload || (res.success && reload) )
                    window.location.reload();
            }
        });
    });
    // \\ ajax запросы при нажатии на кнопки с [data-ajax]
});


/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success)
        $.magnificPopup.close();
});
/* \\ действия по успешной отправке формы через AjaxForm */


/* YANDEX MAPS. Ищет на странице элемент с атрибутом data-map="*адреса разделённые ||*" и вставляет в него карту с маркерами на указанных адресах */
if ( $('[data-map]').length > 0 ) {
    (function maps() {
        var init_map = function($map) {

            var coords_str = $map.data('map'),
            	address_array = coords_str.split('||');

            // расставляем маркеры и центрируем карту
            $.each(address_array, function(idx, addr) {
                ymaps.geocode(addr, {
                    results: 1
                }).then(function(res) {
                	var firstGeoObject = res.geoObjects.get(0),
                        coords = firstGeoObject.geometry.getCoordinates();
                    var placemark = new ymaps.Placemark(coords, {
                    	balloonContent: firstGeoObject.properties._data.balloonContent
                    }, {});

                    if ( typeof myMap !== 'object' ) {
                        myMap = new ymaps.Map($map[0], {
                            center: coords,
                            zoom: 16,
                            controls: ['zoomControl']
                        });

                        if ( $(window).width() < 768 )
                            myMap.behaviors.disable('drag');

                        myMap.behaviors.disable("scrollZoom");
                    }

                    myMap.geoObjects.add(placemark);

                    if (address_array.length > 1) {
                        myMap.setBounds(myMap.geoObjects.getBounds(), {
                            checkZoomRange: true,
                            preciseZoom: true,
                            zoomMargin: 10
                        });
                    } else {
                        if (address_array.length == 1) {
                            myMap.setZoom(16);
                            myMap.setCenter(coords);
                        }
                    }
                });
            });
        }

        // инициализация яндекс карты
        $.getScript('//api-maps.yandex.ru/2.1?lang=ru_RU&apikey=' + $yandex_api_key, function() {
            ymaps.ready(function() {
                $('[data-map]').each(function() {
                    init_map($(this));
                });
            });
        });
    })();
}
/* \\ YANDEX MAPS */
