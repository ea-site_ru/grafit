var syntax        = 'scss', // Syntax: sass or scss;
    gulp          = require('gulp'),
    gutil         = require('gulp-util' ),
    sass          = require('gulp-sass'),
    concat        = require('gulp-concat'),
    uglify        = require('gulp-uglify'),
    cleancss      = require('gulp-clean-css'),
    rename        = require('gulp-rename'),
    autoprefixer  = require('gulp-autoprefixer'),
	notify        = require("gulp-notify"),
	ttf2woff      = require('gulp-ttf2woff'),
	ttf2eot       = require('gulp-ttf2eot'),
	sourcemaps    = require('gulp-sourcemaps'),

	// настройка деплоя: https://webdesign-master.ru/blog/tools/2017-06-13-gulp-rsync.html
	rsync         = require("gulp-rsync"),
	host          = '', // в формате *user*@*host*
	document_root = ''; // полный путь до корневого каталога сайта

gulp.task('styles', function() {
	//return gulp.src('assets/src/styles/**/*.' + syntax)
	return gulp.src('assets/src/styles/common.'+syntax)
	.pipe(sourcemaps.init())
	.pipe(sass({ outputStyle: 'expand' }).on("error", notify.onError()))
	.pipe(rename({ suffix: '.min', prefix : '' }))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
	.pipe(concat('styles.min.css'))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets/css'))
});

gulp.task('scripts', function() {
	return gulp.src([
		'assets/src/scripts/jquery.min.js',
		'assets/src/scripts/swiper.min.js',
		'assets/src/scripts/magnific_popup.min.js',
		'assets/src/scripts/jquery.maskedinput.min.js',
		'assets/src/scripts/common.js', // Always at the end
		])
	.pipe(sourcemaps.init())
	.pipe(concat('scripts.min.js'))
	.pipe(uglify()) // Mifify js (opt.)
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets/js'))
});


// Fonts
gulp.task('ttf', function() {
	return gulp.src('assets/fonts/**/*.ttf')
	.pipe(gulp.dest('assets/fonts'))
});

gulp.task('ttf2eot', function() {
	return gulp.src('assets/fonts/**/*.ttf')
	.pipe(ttf2eot())
	.pipe(gulp.dest('assets/fonts'))
});

gulp.task('ttf2woff', function() {
	return gulp.src('assets/fonts/**/*.ttf')
	.pipe(ttf2woff())
	.pipe(gulp.dest('assets/fonts'))
});

gulp.task('fonts', gulp.parallel('ttf', 'ttf2eot', 'ttf2woff'));


gulp.task('deploy_styles', function() {
	return gulp.src('assets/css/**')
	.pipe(rsync({
		root: 'assets/css/',
		hostname: host,
		destination: document_root + '/assets/css/',
		// include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: true,
		compress: true
  	}))
});

gulp.task('deploy_scripts', function() {
	return gulp.src('assets/js/**')
	.pipe(rsync({
		root: 'assets/js/',
		hostname: host,
		destination: document_root + '/assets/js/',
		// include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: true,
		compress: true
  	}))
});

gulp.task('deploy_fonts', function() {
	return gulp.src('assets/fonts/**')
	.pipe(rsync({
		root: 'assets/fonts/',
		hostname: host,
		destination: document_root + '/assets/fonts/',
		// include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: true,
		compress: true
  	}))
});

gulp.task('deploy_elements', function() {
	return gulp.src('core/elements/**')
	.pipe(rsync({
		root: 'core/elements/',
		hostname: host,
		destination: document_root + '/core/elements/',
		// include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: true,
		compress: true
  	}))
});

gulp.task('deploy_template_images', function() {
	return gulp.src('assets/images/**')
	.pipe(rsync({
		root: 'assets/images/',
		hostname: host,
		destination: document_root + '/assets/images/',
		// include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: true,
		compress: true
  	}))
});

gulp.task('watch', function() {
    gulp.watch([
		'assets/src/styles/*.'+syntax,
		'assets/src/styles/**/*.'+syntax
	], gulp.parallel('styles'));
    gulp.watch([
		'assets/src/scripts/*.js',
		'assets/src/scripts/**/*.js'
	], gulp.parallel('scripts'));

	if ( host != '' && document_root != '' ) {
		gulp.watch([
			'assets/css/*.css',
			'assets/css/**/*.css',
		], gulp.parallel('deploy_styles'));
	}

	if ( host != '' && document_root != '' ) {
		gulp.watch([
			'assets/js/*.js',
			'assets/js/**/*.js'
		], gulp.parallel('deploy_scripts'));
	}

	if ( host != '' && document_root != '' ) {
		gulp.watch([
			'assets/fonts/*',
			'assets/fonts/**/*'
		], gulp.parallel('deploy_fonts'));
	}

	if ( host != '' && document_root != '' ) {
		gulp.watch([
			'core/elements/*',
			'core/elements/**/*'
		], gulp.parallel('deploy_elements'));
	}

	if ( host != '' && document_root != '' ) {
		gulp.watch([
			'assets/images/*'
		], gulp.parallel('deploy_template_images'));
	}
});

gulp.task('default', gulp.parallel('watch', 'styles', 'scripts', 'fonts'));
