<?php

if ( empty($value) ) {
    $crop_sizes = 'w=50&h=50&far=C';
    $padding    = 'padding:0;';
} else
    $crop_sizes = 'w=50&h=50&zc=C';

$photo = $modx->runSnippet('phpthumbon', [
    'input'   => '/uploads/'.$value,
    'options' => $crop_sizes
]);

return '<div style="display:inline-block;font-size:0;background:#FFF;border-radius:50%;margin:0 auto;'.$padding.'border:solid 3px #F5F5F5"><img src="'.$photo.'" style="border-radius:50%"></div>';