<?php

/*
 *  Разделяет входные данные по разделителю и, если указан type равный email, social или phone, оборачивает полученные данные в ссылки.
 *  
 *  параметры:
 *      input                    - входные данные, обязательный параметр
 *      type                     - тип входных данных (address, email, phone, social), если не указан - данные на выходе не будут оборачиваться в ссылки, если указан тип address - будет выводится строка с адресом с подсказкой, указаноой во входных данных через |
 *      show_item                - показать N-ый по порядку элемент
 *      separator                - разделитель (по-умолчанию перенос строки)
 * 
 *      show_messengers          - показывать ссылки на мессенджеры, работает только при type = phone. Поддерживаемые мессенджеры: "|v" - viber, "|wa" - whatsapp
 *      show_address_tip         - показывать подсказку для адресов
 * 
 *      link_class               - CSS-класс ссылки (или наборы CSS-классов через запятую, например: "icon icon_viber,icon icon_whatsapp")
 *      container_class          - CSS-класс обёртки
 *      messengers_icons_classes - CSS-классы иконок менеджеров в JSON-формате (по-умолчанию: {"v":"icon icon_viber","wa":"icon icon_whatsapp"})
 *      address_tip_class        - CSS-класс обёртки подсказки для адреса
 * 
 */

if ( empty($input) ) {
    $modx->log(modX::LOG_LEVEL_ERROR, '[Snippet "SPLITTER"] ERROR: При вызове сниппета не указаны входные данные.');
    return;
}

if ( empty($separator) )
    $separator = PHP_EOL; # разбиваем входные данные по переносу строки
elseif ( $separator == '|' ) {
    $modx->log(modX::LOG_LEVEL_ERROR, '[Snippet "SPLITTER"] ERROR: В качестве разделителя (параметр separator) не может использоваться символ "|".');
    return;
}


# если указано несколько наборов CSS-классов через запятую - формируем массив с CSS-классами
if ( !empty($link_class) )
    if ( strpos($link_class, ',') > 0 ) {
        $link_class = explode(',', $link_class);
        if ( is_array($link_class) && count($link_class) > 0 )
            foreach ( $link_class as $idx => $class)
                $link_classes[$idx] = !empty($class) ? ' class="'.trim($class).'"' : ''; # массив с классами ссылок
    } else {
        $link_classes = !empty($link_class) ? ' class="'.trim($link_class).'"' : ''; # класс ссылки
    }

$container_class = !empty($container_class) ? ' class="'.$container_class.'"' : ''; # класс обёртки


$items = explode($separator, trim($input));

# удаляем пустые элементы
foreach ( $items as $idx => $item )
    if ( empty( $item ) )
        unset( $items[$idx] );

$items = array_values( $items );


if ( $show_item && !$items[$show_item-1] )
    return $modx->log(modX::LOG_LEVEL_WARN, '[Snippet "SPLITTER"] WARNING: Во входных данных не найден элемент под номером '.$show_item.'.');


# если type='phone' - проверяем наличие признаков мессенджера у телефона (например: "|v" - viber, "|wa" - whatsapp)
if ( $type == 'phone' )
    foreach ( $items as $idx => $item) {
        if ( preg_match('/(\|(v|wa))+$/', trim($item), $matches) && !empty($matches[0]) && $show_messengers ) {
            # выкидываем из номера всё кроме цифр
            $messenger_number[$idx] = preg_replace('/[^0-9]/', '', trim( $item) );
            
            # подготавливаем массив с CSS-классами иконок мессенджеров
            $icons_classes = json_decode($messengers_icons_classes, true);
    
            foreach ( explode('|', trim($matches[0], '|')) as $messenger_key ) {
                $icon_class = $icons_classes[$messenger_key] ? ' class="'.$icons_classes[$messenger_key].'"' : '';
                switch ( $messenger_key ) {
                    case 'v':
                        $messengers[$idx] .= '<a href="viber://chat?number='.$messenger_number[$idx].'"'.$icon_class.' target="_blank"></a>';
                    break;
                    case 'wa':
                        $messengers[$idx] .= '<a href="https://wa.me/'.$messenger_number[$idx].'"'.$icon_class.' target="_blank"></a>';
                    break;
                }
            }
        }
        # чистим номер от признаков мессенджера
        $items[$idx] = preg_replace('/[a-z|]/i', '', $item);
    }


$items_list = '';
if (!$show_item) # вывести все элементы из $items
    foreach ($items as $idx => $item) {
        # получаем класс для $idx-ой по порядку ссылки
        if ( is_array($link_classes) && count($link_classes) > 0 )
            $link_class = $link_classes[$idx] ?: $link_classes[0];
        else # если в link_class передан только один набор классов
            $link_class = $link_classes;

        switch( $type ) {
            case 'address':
                list($address, $tip) = explode('|', $item);

                $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).trim($address).( ( $show_address_tip && !empty(trim($tip)) ) ? ' <div class="'.( ( !empty($address_tip_class) ) ? $address_tip_class : 'address__tip' ).'">'.trim($tip).'</div>' : ''  ).( ($container_class) ? '</div>' : '' );
            break;
            case 'email':
                $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).'<a href="mailto:'.trim($item).'"'.$link_class.'>'.trim($item).'</a>'.( ($container_class) ? '</div>' : '' );
            break;
            case 'phone':
                $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).$messengers[$idx].'<a href="tel:'.preg_replace( '/[^0-9+]/', '', trim($item) ).'"'.$link_class.'>'.trim($item).'</a>'.( ($container_class) ? '</div>' : '' );
            break;
            case 'social':
                $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).'<a href="'.trim($item).'"'.$link_class.' target="_blank"></a>'.( ($container_class) ? '</div>' : '' );
            break;
            default:
                $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).trim($item).( ($container_class) ? '</div>' : '' );
            break;
        }
    }
else { # вывести только N-ый элемент из $items
    # получаем класс для $idx-ой по порядку ссылки
    if ( is_array($link_classes) && count($link_classes) > 0 )
        $link_class = $link_classes[$show_item-1] ?: $link_classes[0];
    else # если в link_class передан только один набор классов
        $link_class = $link_classes;

    switch( $type ) {
        case 'address':
            list($address, $tip) = explode('|', $items[$show_item-1]);

            $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).trim($address).( ( $show_address_tip && !empty(trim($tip)) ) ? ' <div class="'.( ( !empty($address_tip_class) ) ? $address_tip_class : 'address__tip' ).'">'.trim($tip).'</div>' : ''  ).( ($container_class) ? '</div>' : '' );
        break;
        case 'email':
            $items_list = ( ($container_class) ? '<div'.$container_class.'>' : '' ).'<a href="mailto:'.trim($items[$show_item-1]).'"'.$link_class.'>'.trim($items[$show_item-1]).'</a>'.( ($container_class) ? '</div>' : '' );
        break;
        case 'phone':
            $items_list = ( ($container_class) ? '<div'.$container_class.'>' : '' ).$messengers[$show_item-1].'<a href="tel:'.preg_replace('/[^0-9+]/', '', trim( $items[$show_item-1]) ).'"'.$link_class.'>'.trim($items[$show_item-1]).'</a>'.( ($container_class) ? '</div>' : '' );
        break;
        case 'social':
            $items_list .= ( ($container_class) ? '<div'.$container_class.'>' : '' ).'<a href="'.trim($items[$show_item-1]).'"'.$link_class.' target="_blank"></a>'.( ($container_class) ? '</div>' : '' );
        break;
        default:
            $items_list = ( ($container_class) ? '<div'.$container_class.'>' : '' ).trim( $items[$show_item-1] ).( ($container_class) ? '</div>' : '' );
        break;
    }
}

return $items_list;