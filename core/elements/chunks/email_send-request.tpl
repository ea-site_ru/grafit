<strong>Имя</strong>: {$name | ucfirst}<br>
<strong>Телефон</strong>: {'!splitter' | snippet : [ 'input' => $phone, 'type' => 'phone' ]}<br>
<strong>Сообщение</strong>:<br>
{$message}<br><br>
Заявка отправлена со страницы <a href="{$page_link}">{$pageId | resource : 'pagetitle'}</a>