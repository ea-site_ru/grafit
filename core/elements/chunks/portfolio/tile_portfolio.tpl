<div class="tile-wrap {if $_modx->resource.template == 2}swiper-slide{else}col-24 col-md-6{/if}">
    <div class="tile tile_portfolio">
        <a href="{$_pls['original_url']}" class="portfolio-tile__link"></a>

        <img src="{$_pls['original_url'] | phpthumbon : 'w=375&h=375&zc=C&q=80'}" class="portfolio-tile__image" alt="{$_pls['name']}">
    </div>
</div>