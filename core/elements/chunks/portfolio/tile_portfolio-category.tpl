<div class="tile-wrap col-24 col-md-6">
    <div class="tile portfolio-category-tile">
        <a href="{$uri}" class="portfolio-category-tile__link"></a>

        <img class="portfolio-category-tile__cover" src="{$cover | phpthumbon : 'w=375&h=375&zc=C&q=80'}" alt="{$pagetitle}">
        <div class="portfolio-category-tile__header">
            {$pagetitle}
        </div>
    </div>
</div>