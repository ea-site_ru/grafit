<div class="row">
    {'!pdoResources' | snippet : [
        'parents'    => 10,
        'limit'      => 12,
        'includeTVs' => 'cover',
        'tvPrefix'   => '',
        'tpl'        => 'tile_service'
    ]}
</div>