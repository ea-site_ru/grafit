<div class="tile-wrap col-24 col-md-12 col-lg-6">
    <div class="tile service-tile">
        <a href="{$uri}" class="service-tile__link"></a>

        <img class="service-tile__cover" src="{$cover | phpthumbon : 'w=375&h=375&zc=C'}" alt="{$pagetitle}">
        <div class="service-tile__header">
            {$pagetitle}
        </div>
    </div>
</div>