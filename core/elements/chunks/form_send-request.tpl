<form action="{$_modx->resource.uri}" method="POST" class="form form_send-request">
    <input type="hidden" name="page_link" value="{$_modx->resource.id | url : ['scheme' => 'full']}">
    
    <div class="form__row">
        <input type="text" class="input input_rounded w-100" name="name" placeholder="Ваше имя">
        <input type="text" class="input input_rounded input_funny" name="fax" placeholder="Ваш факс">
    </div>
    <div class="form__row">
        <input type="text" class="input input_rounded w-100" name="phone" placeholder="+7(___)___-__-__">
    </div>
    <div class="form__row">
        <textarea name="message" class="input input_rounded input_textarea" placeholder="Ваше сообщение"></textarea>
    </div>
    <div class="form__row">
        <label class="custom-checkbox-label">
            <span class="custom-checkbox-wrap">
                <input type="checkbox" name="policy" required>
                <span class="checkbox_custom"></span>
            </span>
            Соглашаюсь на обработку <a href="{9 | url}" class="link link_policy" target="_blank">персональных данных</a>
        </label>
    </div>
    <div class="form__row text-center">
        <button class="btn btn_inverse pt-2">Отправить</button>
    </div>
</form>