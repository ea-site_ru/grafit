<?php
if ($modx->context->key == 'mgr') return;

if ($_SERVER['REQUEST_URI'] !== strtolower($_SERVER['REQUEST_URI']))
    $modx->sendRedirect(strtolower($_SERVER['REQUEST_URI']), array('responseCode' => $_SERVER['SERVER_PROTOCOL'] . ' 301 Moved Permanently'));