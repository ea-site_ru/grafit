<?php
switch ($modx->event->name) {
	case 'OnHandleRequest':
		if ($modx->context->key == 'mgr') { return; }

		$device = 'desktop';

		require_once MODX_BASE_PATH . 'utils/Mobile-Detect/Mobile_Detect.php';
		$detect = new Mobile_Detect;

		if ($detect->isMobile()) {
			$device = 'mobile';
		}

		if ($detect->isTablet()) {
			$device = 'tablet';
		}

		$modx->setPlaceholder('device', $device);

	break;
}