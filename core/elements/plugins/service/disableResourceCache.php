<?php

/** @var modX $modx */
switch ($modx->event->name) {
    case 'OnMODXInit':
        // если системная настройка site_status выключена и настройка cache_resource включена отключаем настройку cache_resource и чистим кэш
        if ( !(int)$modx->config['site_status'] && (int)$modx->config['cache_resource'] ) {
            $setting = $modx->getObject('modSystemSetting', 'cache_resource');
            $setting->set('value', 0);
            $setting->save();
            
            // чистим кэш ресурсов
            $modx->cacheManager->refresh(array('resource' => array()));
        } else { // иначе - включаем настройку cache_resource
            $setting = $modx->getObject('modSystemSetting', 'cache_resource');
            $setting->set('value', 1);
            $setting->save();
        }
        
        // чистим кэш системных настроек
        $modx->cacheManager->refresh(array('system_settings' => array()));
    break;
}