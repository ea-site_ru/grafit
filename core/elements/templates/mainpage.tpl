{extends 'template:layout'}

{block 'content'}
    {* SLIDER *}
    {set $slides = $_modx->runSnippet('!bGallery', [
        'resourceId' => $_modx->resource.id,
        'tpl'        => '@INLINE    <div class="swiper-slide">
                                        {*if $_pls[\'link\']}
                                            <a href="{$_pls[\'link\']}" class="mainpage-slider__slide-link"></a>
                                        {/if*}
                                        <div class="mainpage-slider__image-wrap{if ($_pls[\'name\'] && $_pls[\'name\'] != \'-\') || $_pls[\'description\']} mainpage-slider__image-wrap_blur{/if}" style="background-image:url(\'{$_pls[\'original_url\'] | phpthumbon : \'w=1920&h=1080&zc=C&q=80\'}\')"></div>

                                        <div class="mainpage-slider__slide-info">
                                            {if $_pls[\'name\'] && $_pls[\'name\'] != \'-\'}
                                                <div class="mainpage-slider__slide-header">{$_pls[\'name\']}</div>
                                            {/if}
                                            {if $_pls[\'description\']}
                                                <div class="mainpage-slider__slide-descr">{$_pls[\'description\']}</div>
                                            {/if}
                                            {if $_pls[\'link\']}
                                                {set $link_attributes = $_pls[\'link\'] | split : \'|\'}
                                                <a href="{$link_attributes[0]}" class="mainpage-slider__slide-more-link"{if $link_attributes[0] | preg_match : \'/^#/\' } data-popup{/if}>{$link_attributes[1] ?: \'Узнать подробности...\'}</a>
                                            {/if}
                                        </div>
                                    </div>'
    ])}
    
    {if $slides}
    <div id="mainpage_slider" class="block block_mainpage-slider block_without-border block_no-padding swiper-container">
        <div class="swiper-wrapper">
            {$slides}
        </div>
        
        {if 'bGalTotal' | placeholder > 1}
            <div class="mainpage-slider__arrows">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            
            {* <div class="swiper-pagination"></div> *}
        {/if}

        <div class="icon_scroll-down">
            {if 'device' | placeholder != 'desktop'}
                <img src="/assets/images/smartphone.png" alt="">
            {else}
                <img src="/assets/images/pc_mouse.png" alt="">
            {/if}
        </div>
    </div>
    {/if}
    {* \\ SLIDER *}
    
    
    {*if $_modx->hasSessionContext('mgr')}
        <div class="block container">
            <div class="block__header">
                Пример обработки AJAX-запросов с помощью плагина AJAX
            </div>
            <div class="block__content">
                <form id="form" action="{$_modx->resource.id | url}" method="post" enctype="multipart/form-data" class="container">
                    <div class="form__content row">
                        <input type="hidden" name="data" value="value">
                        <input type="text" class="input" name="input">
                        <input type="file" name="file">
                        <input type="submit" value="Отправить"{* form="form"* } data-ajax="phones_list">
                    </div>
                    
                    <div class="form__message"></div>
                </form>
            </div>
        </div>
    {/if*}
    

    <div id="{10 | resource : 'alias'}" class="block block_without-border container">
        <div class="block__content">
            {include 'block_service-tiles'}
        </div>
    </div>


    {if $about_company = $_modx->resource.content ?: 5 | resource : 'introtext' ?: 5 | resource : 'content'}
        <div id="{5 | resource : 'alias'}" class="block container">
            <div class="block__header">
                {5 | resource : 'pagetitle'}
            </div>
            <div class="block__content">
                {$about_company}
            </div>
            {if !$_modx->resource.content && (5 | resource : 'content')}
            <div class="block__footer text-right">
                <a href="{5 | url}" class="link_read-more">Подробнее...</a>
            </div>
            {/if}
        </div>
    {/if}
    

    {*set $portfolio = '!pdoPage' | snippet : [
        'parents'    => 6,
        'templates'  => 8,
        'limit'      => 10,

        'includeTVs' => 'cover',
        'tvPrefix'   => '',
        'prepareTVs' => 1,

        'sortby'     => '{"menuindex":"ASC"}',

        'tpl'        => 'tile_portfolio'
    ]*}
    {set $portfolio = '!bGallery' | snippet : [
        'resourceId' => '24, 25',
        'limit'      => 20,
        'tpl'        => 'tile_portfolio'
    ]}
    {if $portfolio}
    <div id="{6 | resource : 'alias'}" class="block block_dark">
        <div class="container">
            <div class="block__header">
                {6 | resource : 'longtitle' ?: 6 | resource : 'pagetitle'}
            </div>
            <div id="mainpage_portfolio_slider" class="block__content swiper-container">
                <div class="swiper-wrapper" data-popup="gallery">
                    {$portfolio}
                </div>

                {*
                <div class="mainpage-portfolio-slider__arrows">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
                *}
                
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
    {/if}


    <div id="{12 | resource : 'alias'}" class="block container">
        <div class="block__header">
            {12 | resource : 'pagetitle'}
        </div>
        <div class="block__content">
            <ol class="working-process row">
                {foreach 1 | resource : 'working_process' | json_decode as $item }
                    <li class="working-process__item col-24 col-lg-5">
                        <div class="working-process__item-header">
                            {$item.header}
                        </div>
                        <div class="working-process__item-descr">
                            {$item.descr}
                        </div>
                    </li>
                {/foreach}
            </ol>
        </div>
    </div>

    {if 11 | resource : 'introtext'}
        <div id="{11 | resource : 'alias'}" class="block block_matching container">
            <div class="block__header">
                {11 | resource : 'longtitle' ?: 11 | resource : 'pagetitle'}
            </div>
            <div class="block__content">
                <div class="row">
                    <div class="col-24 col-md-16">
                        {11 | resource : 'introtext'}
                    </div>
                </div>
            </div>
            {if 11 | resource : 'content'}
            <div class="block__footer text-right">
                <div class="row">
                    <div class="col-24 col-md-16">
                        <a href="{11 | url}" class="link_read-more">Подробнее...</a>
                    </div>
                </div>
            </div>
            {/if}
        </div>
    {/if}
{/block}