{extends "template:layout"}

{block 'content'}
    <div class="block block_without-border container">
        <h1 class="block__header">
            {10 | resource : 'pagetitle'}
        </h1>
        <div class="block__content">
            {include 'block_service-tiles'}
        </div>
    </div>
{/block}