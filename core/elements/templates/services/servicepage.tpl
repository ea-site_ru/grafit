{extends 'template:layout'}

{block 'content'}
    <div class="block block_without-border block_no-padding container">
        <h1 class="block__header">
            {$_modx->resource.pagetitle}
        </h1>
        <div class="block__content">
            <img src="{$_modx->resource.cover | phpthumbon : 'w=400&h=400&zc=C'}" alt="" class="service-page__cover">
            {$_modx->resource.content}
        </div>
        <div class="block__footer text-center">
            <a href="#send_request" class="btn btn_accent" data-popup="">
                Хочу {$_modx->resource.pagetitle_in_genitive}!
            </a>
        </div>
    </div>
{/block}