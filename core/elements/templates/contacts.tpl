{extends 'template:layout'}

{block 'content'}
    <div class="block block_without-border container">
        <h1 class="block__header">
            {$_modx->resource.pagetitle}
        </h1>
        <div class="block__content">
            <div class="contacts-page__contacts">
                {if 'cfg_phones' | option}
                    <div class="contacts-page__contacts-row row">
                        <div class="contacts-page__contacts-label col-6 col-xl-4">Тел.:</div>
                        <div class="col">
                            {'!splitter' | snippet : [
                                'type'            => 'phone',
                                'input'           => 'cfg_phones' | option,
                                'show_messengers' => 1,
                                'container_class' => 'contacts-page__contacts-phone-wrap'
                            ]}
                        </div>
                    </div>
                {/if}
                
                {if 'cfg_emails' | option}
                    <div class="contacts-page__contacts-row row">
                        <div class="contacts-page__contacts-label col-6 col-xl-4">E-mail:</div>
                        <div class="col">
                            {'!splitter' | snippet : ['type' => 'email', 'input' => 'cfg_emails' | option]}
                        </div>
                    </div>
                {/if}
                
                {if 'cfg_addresses' | option}
                    <div class="contacts-page__contacts-row row">
                        <div class="contacts-page__contacts-label col-24 col-md-6 col-xl-4">Адрес:</div>
                        <div class="col">
                            {'!splitter' | snippet : [
                                'type'              => 'address',
                                'input'             => 'cfg_addresses' | option,
                                'show_address_tip'  => 1,
                                'show_item'         => 1,
                                'container_class'   => 'contacts-page__contacts-address-wrap',
                                'address_tip_class' => 'contacts-page__contacts-address-tip'
                            ]}
                        </div>
                    </div>
                {/if}
                
                {if 'cfg_opentime' | option}
                    <div class="contacts-page__contacts-row row">
                        <div class="contacts-page__contacts-label col-24 col-md-6 col-xl-4">Режим работы:</div>
                        <div class="col">
                            {'cfg_opentime' | option}
                        </div>
                    </div>
                {/if}
            </div>

            {if $_modx->resource.content && ('cfg_contacts_page_content_position' | option) == 'before_map'}
                <div class="my-5">
                    {$_modx->resource.content}
                </div>
            {/if}
        </div>
    </div>
    
    {set $address = ('!splitter' | snippet : [
        'type'              => 'address',
        'input'             => 'cfg_addresses' | option,
        'show_address_tip'  => 0,
        'show_item'         => 1
    ]) | replace : '&nbsp;' : ' '}
        
    {if ('cfg_show_contacts_page_map' | option) && $address}
        <div class="block block_without-border">
            <div class="block__content">
                <div data-map="{( ('cfg_addresses_coords' | option) | esplit : '/\n/' | join : '||' ) ?: $address | esplit : '/\n/' | join : '||'}"></div>
            </div>
        </div>
    {/if}

    {if $_modx->resource.content && ('cfg_contacts_page_content_position' | option) == 'after_map' }
        <div class="block block_without-border container">
            <div class="block__content">
                {$_modx->resource.content}
            </div>
        </div>
    {/if}
{/block}