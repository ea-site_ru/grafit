{extends 'template:layout'}

{block 'content'}
    <div class="block block_without-border block_no-padding container">
        <h1 class="block__header">
            {$_modx->resource.pagetitle}
        </h1>
        <div class="block__content">
            {$_modx->resource.introtext}
            {$_modx->resource.content}
        </div>
    </div>
{/block}