{* Версия CSS-стилей и JS-скриптов *}
{set $assetsVer = 0.1}


<!doctype html>
<html lang="ru" class="h-100" data-device="{'device' | placeholder}">
    <head>
        <title>{$_modx->resource.longtitle ?: 'site_name' | option ~ ' - ' ~ $_modx->resource.pagetitle}</title>
        <base href="{'site_url' | option}" />
        <meta charset="{'modx_charset' | option}" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        
        <meta name="keywords" content="{$_modx->resource.id | resource : 'seo_keywords'}">
        <meta name="description" content="{$_modx->resource.id | resource : 'seo_descr'}">
        
        <link rel="stylesheet" href="/assets/css/styles.min.css?v={$assetsVer}">
        <link rel="preload" href="/assets/js/scripts.min.js?v={$assetsVer}" as="script">

        <link rel="canonical" href="{$_modx->resource.id | url : ['scheme' => 'full']}">

        <meta name="author" content="http://ea-site.ru"/>
    </head>
    <body class="{if $_modx->resource.id == 'site_start' | option}mainpage {elseif $_modx->resource.template == 6}contacts-page {/if}d-flex flex-column h-100">
        <div id="preloader" class="preloader">
            {if 'cfg_logo' | option}
                <img src="{'cfg_logo' | option | phpthumbon : 'w=200&far=C'}" class="preloader__logo" alt="{'site_name' | option}">
            {else}
                <span class="preloader__site-name">
                    Grafit
                </span>
            {/if}

            {*
            <span class="preloader__message">
                Загрузка...
            </span>
            *}
        </div>
        
        <header class="header fixed-top">
            <div class="header__panel header__panel_top container">
                <div class="row align-items-center">
                    <div class="header__logo-wrap col">
                        {if $_modx->resource.id != 'site_start' | option} {* не выводим ссылку на главную на главной *}
                        <a href="{'site_start' | option | url}" class="header__logo-link">
                        {/if}
                            {if 'cfg_logo' | option}
                                <img src="{'cfg_logo' | option | phpthumbon : 'h=64&far=C'}" class="header__logo" alt="{'site_name' | option}">
                            {else}
                                <span class="header__site-name">
                                    Grafit
                                </span>
                            {/if}
                        {if $_modx->resource.id != 'site_start' | option}
                        </a>
                        {/if}

                        {if $_modx->resource.id != 'site_start' | option} {* не выводим ссылку на главную на главной *}
                        <a href="{'site_start' | option | url}" class="header__logo-link">
                        {/if}
                            {if 'cfg_slogan' | option}
                                <span class="header__slogan">
                                    {'cfg_slogan' | option}
                                </span>
                            {/if}
                        {if $_modx->resource.id != 'site_start' | option}
                        </a>
                        {/if}
                    </div>
                    
                    
                    <div class="header__mobile-menu-btn-wrap col-4 d-md-none">
                        <button class="btn btn_plain btn_menu" data-toggle="#header_nav"></button>
                    </div>
                    
                    
                    <div class="header__phones d-none d-md-flex col-md-10 col-lg-8">
                        {* проверяем наличе "|v|wa" или "|wa|v" в первом телефоне *}
                        {set $tmp = 'cfg_phones' | option | split : '|'}

                        {if $tmp[1] != 'v' && $tmp[1] != 'wa'}
                            {set $phone_icon_class = ' icon_phone'}
                        {/if}
                        {* \\ проверяем наличе "|v|wa" или "|wa|v" в первом телефоне *}

                        {$header_phones = '!splitter' | snippet : [
                            'input'                    => 'cfg_phones' | option,
                            'type'                     => 'phone',
                            'messengers_icons_classes' => '{"v":"icon_viber","wa":"icon_whatsapp"}',
                            'container_class'          => 'header__phone-wrap text-right',
                            'link_class'               => 'header__phone' ~ $phone_icon_class,
                            'show_messengers'          => 1,
                            'show_item'                => 1
                        ]}
                    </div>
                    
                    {*
                    <div class="header__emails d-none d-md-block col-md-6 col-lg-4">
                        {'!splitter' | snippet : [
                            'input'           => 'cfg_emails' | option,
                            'type'            => 'email',
                            'container_class' => 'header__email-wrap text-right',
                            'link_class'      => 'btn btn_plain header__email',
                            'show_item'       => 1
                        ]}
                    </div>
                    *}
                </div>
            </div>
            
            <div class="header__panel header__panel_bottom">
                <div class="container">
                    <div class="header__phones-mobile-wrap d-md-none">
                        {$header_phones}
                    </div>
                    <div class="header__nav-wrap">
                        {'!pdoMenu' | snippet : [
                            'parents'    => 0,
                            'resources'  => '-12',
                            'level'      => 1,

                            'outerClass' => 'header__nav d-none d-md-flex col',
                            'rowClass'   => 'header__nav-item',

                            'tplOuter'   => '@INLINE    <ul id="header_nav" {$classes}>{$wrapper}</ul>',
                            'tplInner'   => '@INLINE    <ul {$classes}>{$wrapper}</ul>',
                            'tpl'        => '@INLINE    <li {$classes}>
                                                            {* если мы не на главной и у ссылки в атрибутах прописано data-scroll-to => перекидываем на главную и мотаем страницу до нужного блока *}
                                                            {*if $_modx->resource.template != 2 && $attributes | preg_match : \'/^data-scroll-to/\'}
                                                                {set $link = \'/#\' ~ $alias}
                                                            {/if*}

                                                            <a href="{$link}" class="header__nav-link" {$attributes}>{$menutitle}</a>
                                                            {$wrapper}
                                                        </li>'
                        ]}
                    </div>
                    <a href="#send_request" class="btn btn_plain btn_header-send-request" data-popup>Оставить заявку</a>
                </div>
            </div>

            {* <a href="#popup" data-popup>Открыть тестовое всплывающее окно</a> *}
        </header>
        
        <main class="flex-shrink-0">
            {block 'breadcrumbs'}
                {if 'device' | placeholder == 'mobile'}
                    {set $showCurrent = 0}
                    {0 | setPlaceholder : 'skip'}
                {else}
                    {set $showCurrent = 1}
                {/if}

                {'!pdoCrumbs' | snippet : [
                    'showHome'        => 1,
                    'showCurrent'     => $showCurrent,
                    'showAtHome'      => 0,
                    'hideSingle'      => 1,
                
                    'tplWrapper'      => '@INLINE   <ul class="breadcrumbs container">{$output}</ul>',
                    'tplHome'         => '@INLINE   <li class="breadcrumbs__item breadcrumbs__item_home"><a href="{$link}" class="breadcrumbs__link icon icon_home"></a></li>',
                    'tpl'             => '@INLINE   {if ( $id == $_modx->resource.parent && (\'device\' | placeholder == \'mobile\') ) || (\'device\' | placeholder) != \'mobile\'}
                                                        <li class="breadcrumbs__item"><a href="{$link}" class="breadcrumbs__link">{$menutitle}</a></li>
                                                    {elseif ($_modx->resource.parent | resource : \'parent\' != 0) && !(\'skip\' | placeholder)}
                                                        <li class="breadcrumbs__item">...</li>
                                                        {1 | setPlaceholder : \'skip\'}
                                                    {/if}',
                    'tplCurrent'      => '@INLINE   <li class="breadcrumbs__item active">{$menutitle}</li>',
                    'outputSeparator' => '<li class="breadcrumbs__item breadcrumbs__item_separator">&bull;</li>'
                ]}
            {/block}

            {block 'content'}
                {$_modx->resource.content}
            {/block}
        </main>
        
        {*if $_modx->resource.template != 6}
            <div class="block">
                <div class="block__content" data-map="{$_modx->config.cfg_addresses | esplit : '/\n/' | join : '||'}"></div>
            </div>
        {/if*}
        
        <footer class="footer mt-auto">
            <div class="container">
                <div class="row">
                    <div class="footer__address col-24 col-md text-center text-md-left">
                        {'cfg_addresses' | option}
                    </div>  
                    <div class="footer__contacts col-24 col-md-6 text-center">
                        {'!splitter' | snippet : [
                            'input'           => 'cfg_phones' | option,
                            'type'            => 'phone',
                            'container_class' => 'footer__phone-wrap',
                            'link_class'      => 'footer__phone icon_phone',
                            'show_item'       => 1
                        ]}
                    </div>
                    <div class="footer__copyrights col-24 col-md-6 text-center text-md-right">
                        {'' | date : 'Y'} &copy; {'site_name' | option}
                    </div>
                </div>
            </div>
        </footer>
        
        
        {* POPUPS *}
        <div id="send_request" class="mfp-hide white-popup">
            <div class="container">
                <div class="row">
                    <div class="popup col-24 col-md-18 offset-md-3 col-lg-12 offset-lg-6">
                        <div class="popup__header">
                            Оставить заявку
                        </div>
                        <div class="popup__content">
                            {'!AjaxForm' | snippet : [
                                'frontend_css'           => '/assets/css/ajaxForm.css',
                                'form'                   => 'form_send-request',
                                'formName'               => 'Форма "Оставить заявку"',
                                'hooks'                  => 'FormItSaveForm, email',
                                'emailSubject'           => 'Новая заявка с сайта "' ~ ('site_name' | option) ~ '"',
                                'emailTo'                => 'cfg_feedback_email' | option,
                                'emailFrom'              => 'cfg_email_from' | option,
                                'emailTpl'               => 'email_send-request',
                                'validate'               => 'name:required,phone:required,fax:blank,message:required,page_link:required',
                                'validationErrorMessage' => 'Заполните обязательные поля формы!',
                                'formFields'             => 'name,phone,message,page_link',
                                'fieldNames'             => 'name==Имя,phone==Телефон,message==Сообщение,page_link==Страница с которой отправлена форма',
                                'successMessage'         => 'В ближайшее время Вам перезвонят.'
                            ]}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {* \\ POPUPS *}
        
        {'cfg_counters' | option | replace : '{' : '{ '}
        
        <script>
            var $yandex_api_key = "{'cfg_yandex_api_key' | option}";
        </script>

        <script src="/assets/js/scripts.min.js?v={$assetsVer}"></script>
    </body>
</html>