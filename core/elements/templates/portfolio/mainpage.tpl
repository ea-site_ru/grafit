{extends "template:layout"}

{block 'content'}
    <div class="block block_without-border container">
        <h1 class="block__header">
            {$_modx->resource.menutitle ?: $_modx->resource.pagetitle}
        </h1>
        <div class="block__content row">
            {'!pdoPage' | snippet : [
                'depth'            => 0,
                'includeTVs'       => 'cover',
                'tvPrefix'         => '',
                'where'            => '{"class_key":"modDocument"}',
                'limit'            => 8,
                'pageLimit'        => 7,

                'tplPageWrapper'   => '@INLINE <ul class="pagination">{$prev}{$pages}{$next}</ul>',

                'tplPage'          => '@INLINE <li class="pagination__item"><a href="{$href}" class="pagination__link">{$pageNo}</a></li>',
                
                'tplPagePrev'      => '@INLINE <li class="pagination__item pagination__item_control"><a href="{$href}" class="pagination__link">Назад</a></li>',
                'tplPageNext'      => '@INLINE <li class="pagination__item pagination__item_control"><a href="{$href}" class="pagination__link">Вперёд</a></li>',
                'tplPagePrevEmpty' => '@INLINE <li class="pagination__item pagination__item_control disabled"><span>Назад</span></li>',
                'tplPageNextEmpty' => '@INLINE <li class="pagination__item pagination__item_control disabled"><span>Вперёд</span></li>',
                
                'tplPageSkip'      => '@INLINE <li class="pagination__item pagination__item_skip disabled"><span>...</span></li>',
                
                'tplPageActive'    => '@INLINE <li class="pagination__item active"><span>{$pageNo}</span></li>',
                'tpl'              => 'tile_portfolio-category'
            ]}
        </div>

        {if 'page.nav' | placeholder}
            <div class="block__footer row">
                {'page.nav' | placeholder}
            </div>
        {/if}
    </div>
{/block}