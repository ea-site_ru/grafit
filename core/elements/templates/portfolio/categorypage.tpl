{extends 'template:layout'}

{block 'content'}
    <div class="block block_without-border block_no-padding container">
        <h1 class="block__header">
            {$_modx->resource.pagetitle}
        </h1>
        <div class="block__content row" data-popup="gallery">
            {'!bGallery' | snippet : [
                'resourceId' => $_modx->resource.id,
                'tpl'        => 'tile_portfolio'
            ]}
        </div>
    </div>
{/block}